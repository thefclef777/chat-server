#-------------------------------------------------
#
# Project created by QtCreator 2015-08-22T00:46:14
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = chat_server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    chatserver.cpp

HEADERS += \
    server.h \
    chatserver.h
