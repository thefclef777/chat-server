#include "server.h"

static inline quint64 ArrayToInt(QByteArray source);

Server::Server(QObject *parent, int port) : QObject(parent)
{
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), SLOT(newConnection()));
    if(server->listen(QHostAddress::Any, port))
    {
        qDebug()<<"Server Listening";
    }
    else
    {
        qDebug()<<"Failed to open port";
    }
}

void Server::newConnection()
{
    while (server->hasPendingConnections())
    {
        QTcpSocket *socket = server->nextPendingConnection();
        connect(socket, SIGNAL(readyRead()), SLOT(readyRead()));
        connect(socket, SIGNAL(disconnected()), SLOT(disconnected()));
        QByteArray *buffer = new QByteArray();
        quint64 *s = new quint64(0);
        buffers.insert(socket, buffer);
        sizes.insert(socket, s);
        QByteArray newPerson = QString("SRVMSG::New Client: "+socket->peerAddress().toString()).toLocal8Bit();
        propagate(newPerson.toBase64(),NULL);
    }
}

void Server::disconnected()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);
    quint64 *s = sizes.value(socket);
    buffers.remove(socket);
    delete buffer;
    delete s;
}

QByteArray IntToArray(quint64 source)
{
    QByteArray temp;
    QDataStream data(&temp, QIODevice::ReadWrite);
    data << source;
    return temp;
}

bool Server::writeData(QTcpSocket * socket, QByteArray data)
{
    try{
        if(socket != NULL)
        {
                socket->write(IntToArray(data.size())); //write size of data
                socket->write(data); //write the data itself
                return socket->waitForBytesWritten();
        }
        return false;
        } catch(...)
    {
        return false;
    }
}


void Server::readyRead()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);
    quint64 *s = sizes.value(socket);
    quint64 size = *s;
    while (socket->bytesAvailable() > 0)
    {
        buffer->append(socket->readAll());
        while ((size == 0 && (quint64) buffer->size() >= 8) || (size > 0 && (quint64) buffer->size() >= size))
        {
            if (size == 0 && (quint64) buffer->size() >= 8)
            {
                size = ArrayToInt(buffer->mid(0, 8));
                *s = size;
                buffer->remove(0, 8);
            }
            if (size > 0 && (quint64) buffer->size() >= size)
            {
                QByteArray data = buffer->mid(0, size);
                buffer->remove(0, size);
                size = 0;
                *s = size;
                propagate(data,socket);
            }
        }
    }
}

void Server::propagate(QByteArray data,QTcpSocket * sock)
{
    QList<QTcpSocket*> sockets = buffers.keys();
    for(int i = 0; i<sockets.size();i++)
    {
        if(sockets[i] != sock)
        {
            writeData(sockets[i],data);
        }
    }
}

quint64 ArrayToInt(QByteArray source)
{
    quint64 temp;
    QDataStream data(&source, QIODevice::ReadWrite);
    data >> temp;
    return temp;
}



