#include "chatserver.h"
#include "server.h"
#include <QtCore>

ChatServer::ChatServer(QObject * parent,int port) : QObject(parent)
{
    server = new Server(new QObject(), port);
    QObject::connect(server,SIGNAL(dataReceived(QByteArray)),this,SLOT(recievedData(QByteArray)));
    QObject::connect(server,SIGNAL(dataReq(QTcpSocket*)),this,SLOT(sendData(QTcpSocket*)));
}

void ChatServer::recievedData(QByteArray data)
{
}

void ChatServer::sendData(QTcpSocket * socket)
{
}

ChatServer::~ChatServer()
{
    delete server;
}
