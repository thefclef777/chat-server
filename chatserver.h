#ifndef CHATSERVER_H
#define CHATSERVER_H

#include "server.h"
#include <QtCore>

class ChatServer: public QObject
{
    Q_OBJECT
public:
    explicit ChatServer(QObject * parent=0, int port=5000);
    ~ChatServer();
public slots:
    void recievedData(QByteArray data);
    void sendData(QTcpSocket * socket);
private:
    Server* server;
    QList<QByteArray> msgs;
};

#endif // CHATSERVER_H
