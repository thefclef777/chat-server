#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QtCore>
#include <QtNetwork>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0, int port = 5000);
    bool writeData(QTcpSocket*,QByteArray);
    void propagate(QByteArray data, QTcpSocket *sock);

signals:
    void dataReceived(QByteArray);
    void dataReq(QTcpSocket *);

private slots:
    void newConnection();
    void disconnected();
    void readyRead();

private:
    QTcpServer *server;
    QHash<QTcpSocket*, QByteArray*> buffers; //We need a buffer to store data until block has completely received
    QHash<QTcpSocket*, quint64*> sizes; //We need to store the size to verify if a block has received completely
};

#endif // MYTCPSERVER_H
