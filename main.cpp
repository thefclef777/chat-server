#include <QCoreApplication>
#include "server.h"
#include "chatserver.h"
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if(argc>=2)
    {
        if(QString(argv[1]) == "-p")
        {
            ChatServer * chatserver = new ChatServer(new QObject(),atoi(argv[2]));
        }
        else if(QString(argv[1]) == "--help")
        {
            qDebug() << "Cryptochat Server";
            qDebug() << " -p\t Specify listening port (Default: 5000)";
            qDebug() << "\n";
            return 0;
        }
        else
        {
            qDebug()<<"Invalid Input";
        }
    }
    else
    {
        ChatServer * chatserver = new ChatServer(new QObject(),5000);
    }
    return a.exec();
}
